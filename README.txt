
PROTECT PERMISSIONS
===================


MOTIVATION
----------

Drupal has two extremely powerful permissions:

 - 'administer permissions' allows assigning all permissions to any role,
   even to the anonymous user role! Specifically, a user with this 
   permission can obtain any other permission for himself.

 - 'administer users' allows editing any user, including his own and even 
   user 1. This includes 
    . assigning/removing any roles (and thus any permissions),
    . changing the user's name (to cover his tracks),
    . changing the user's password (to downright hijack an account), or
    . changing the email address (to obtain a login link to gain access
      to the other user's powers without his knowledge).
    . It also allows deleting any user, including user 1, which can seriously
      disrupt the site's operation.

This means that either of these permissions allows obtaining complete control
over your site, and you as administrator or consultant may be reluctant to
give a role with either of these permissions to someone else, because
 - you may not trust their Drupal competence,
 - you may not trust their integrity, and/or
 - you may not trust their ability to keep their account safe from others.

However, as long as you withhold these permissions, you will never have an
assistant or successor who can learn how to do your job. It becomes obvious
that we need safe versions of these permissions that we can grant to others
without putting the entire site at stake, in order to get help managing
normal users and to introduce our assistants to using the permissions page
by giving them a safe subset.

This module provides an install-it-and-forget-it solution that is in many
ways superior to solutions that require you to define and continually audit
and maintain explicitly who can do what.



NO ENDORSEMENT
--------------

The Drupal Security Team does not provide endorsements for contrib modules.
Their stand is that it's the administrator's job to safe-guard the
'administer permissions' and 'administer users' permissions, and that a
site where either of these permissions is compromised is a broken site.

As with all contrib modules, especially those that relate to security, you
can rely on the community for testing and feedback, but ultimately, you
yourself are responsible for determining whether it meets your requirements.



HOW IT WORKS
------------

The Protect Permissions (PP) module automatically protects all the security-
relevant permissions using the following rules:

1. PP sanitizes the 'administer users' permission for users who have
   'administer users' but not 'administer permissions' ("deputy admins").
   Users without the former pose no risk, and users with the latter are all-
   powerful anyway.

2. PP considers and affects only permissions that are marked as having
   security implications, in core or contrib.

3. Deputy admins cannot assign or remove roles that carry security-
   relevant permissions, which they don't hold themselves.

4. In addition, deputy admins cannot edit user names, email addresses,
   nor passwords of accounts that hold security-relevant permissions,
   which they don't have themselves, nor can they delete any such accounts.
   This ensures that deputy admins (or someone who stole such an account)
   cannot obtain additional permissions or disrupt the operation of a site
   through hacking.

That's the basic PP functionality. It automatically takes the security and
manageability of your site to a new level simply by installing PP. The
following items provide additional related goodies:

5. The new 'administer user settings' permission governs access to the
   admin/config/people/accounts pages. Access to these pages is not required
   for daily operation.

   One setting on the admin/config/people/accounts/settings page, the
   Administrator role selection, carries a much higher security risk than
   all the others. We think this selection should be restricted to the
   'administer permissions' permission, and we consider this to be a bug
   in D7. The Fix Core module for D7 has a fix for this.

6. The optional new 'administer safe permissions' permission allows you to
   grant selected deputy admins access to a sanitized permissions page (with
   all permissions grayed out that have security implications), so that they
   can add or remove non-security-relevant permissions to/from any role and
   learn how use that page.

7. The optional new 'administer own permissions' permission works like the
   'administer safe permissions', but it makes a different subset of
   permissions available: those that the user himself holds, including the
   (restricted) 'administer users' permission, but excluding the two new
   'administer xyz permissions' permissions.

8. Either of the last two permissions allows creating and deleting roles,
   as long as these roles do not carry inaccessible permissions.

9. The PP module protects itself against being disabled by anyone but
   user 1 and users holding the core administrator role.
   They can be granted separately or together.



INSTALLATION
------------

1. Copy the entire 'protect_permissions' directory, containing the
'protect_permissions.module' and other files, to your Drupal contrib modules
directory.

2. Log in as site administrator.

3. Go to the administration page for modules and enable the module.



CONFIGURATION
-------------

There is no configuration necessary. If you want, you can assign the new
sanitized
 - 'administer user settings',
 - 'administer safe permissions', and
 - 'administer own permissions'
permissions.
