<?php

/**
 * @file
 * User helper functions for the Protect Permissions module.
 */

function _protect_permissions_process_checkboxes($element, $form_state) {
  $cps = _protect_permissions_get_critical_permissions();
  $ups = _protect_permissions_get_user_permissions();
  $allow_safe = user_access('administer safe permissions');
  $allow_own = user_access('administer own permissions');
  foreach (element_children($element) as $perm) {
    if ($allow_safe && (array_search($perm, $cps) === FALSE)) {
      continue;
    }
    if ($allow_own && (array_search($perm, $ups) !== FALSE)) {
      continue;
    }
    $element[$perm]['#access'] = FALSE;
  }
  return $element;
}

function _protect_permissions_process_checkbox($element, $form_state) {
  $cps = _protect_permissions_get_critical_permissions();
  $ups = _protect_permissions_get_user_permissions();
  $allow_safe = user_access('administer safe permissions');
  $allow_own = user_access('administer own permissions');
  $perm = $element['#return_value'];
  if (array_search($perm, array('administer safe permissions', 'administer own permissions')) === FALSE) {
    if ($allow_safe && (array_search($perm, $cps) === FALSE)) {
      return $element;
    }
    if ($allow_own && (array_search($perm, $ups) !== FALSE)) {
      return $element;
    }
  }
  $element['#access'] = FALSE;
  //dpm($element, "element {$perm} removed");
  return $element;
}

function _protect_permissions_get_critical_permissions($reset = FALSE) {
  $critical_permissions = &drupal_static(__FUNCTION__);
  if ($reset || !isset($critical_permissions)) {
    $permissions = module_invoke_all('permission');
    foreach ($permissions as $key => $permission) {
      if (!empty($permission['restrict access'])) {
        $critical_permissions[] = $key;
      }
    }
    $critical_permissions[] = 'administer safe permissions';
    $critical_permissions[] = 'administer own permissions';
    drupal_alter('protect_permissions_critical_permissions', $critical_permissions);
  }
  return $critical_permissions;
}

function _protect_permissions_get_user_permissions($reset = FALSE) {
  global $user;
  $user_permissions = &drupal_static(__FUNCTION__);
  if ($reset || !isset($user_permissions)) {
    $user_permissions = array();
    foreach (user_role_permissions($user->roles) as $perms) {
      $user_permissions += $perms;
    }
    $user_permissions = array_keys($user_permissions);
    //dpm($user_permissions);
  }
  return $user_permissions;
}

/**
 * Really implements hook_form_alter().
 *
 * Adds checkboxes for assignable roles to the user edit form.
 *
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function _protect_permissions_user_form_alter(array &$form, array &$form_state, $form_id) {
  // Get all roles that are available.
  $roles = user_roles(TRUE);

  // Get roles that are available for assignment.
  $available_roles = _protect_permissions_available_roles($roles);

  // Get roles already assigned to the account.
  $account = menu_get_object('user');
  $assigned_roles = (isset($account->roles) ? $account->roles : array());

  // An account might already have a role that isn't available for assignment
  // through this module. Such a role is called "sticky".
  // Get sticky roles.
  $sticky_roles = array_diff($assigned_roles, $available_roles);
  $sticky_roles = array_intersect_key($roles, $sticky_roles);

  // Store sticky roles for later use in roleassign_user_presave().
  _protect_permissions_sticky_roles($sticky_roles);

  // Make a string of all sticky roles.
  $sticky_roles[DRUPAL_AUTHENTICATED_RID] = $roles[DRUPAL_AUTHENTICATED_RID];
  $sticky_roles_str = implode(', ', $sticky_roles);

  // Build the assign roles checkboxes.
  $roles_field = array(
    '#type' => 'checkboxes',
    '#title' => t('Available roles'),
    '#options' => $available_roles,
    '#default_value' => array_keys($assigned_roles),
    '#description' => t('The user receives the combined permissions of all roles selected here and the following roles: %roles.', array('%roles' => $sticky_roles_str)),
  );

  // The user form is sometimes within an 'account' fieldset.
  if (isset($form['account'])) {
    $user_form =& $form['account'];
  }
  else {
    $user_form =& $form;
  }

  // Add the assign roles checkboxes to the user form, and make sure
  // that the notify user checkbox comes last.
  if (isset($user_form['notify'])) {
    $notify_field = $user_form['notify'];
    unset($user_form['notify']);
    $user_form['roleassign_roles'] = $roles_field;
    $user_form['notify'] = $notify_field;
  }
  else {
    $user_form['roleassign_roles'] = $roles_field;
  }

  if (isset($account) && count(_protect_permissions_available_roles($account->roles)) < count($account->roles) - 1) {
    drupal_set_message(t('This user is protected &mdash; some of the fields are locked.'), 'warning', FALSE);
    $form['account']['name']['#disabled'] = TRUE;
    $form['account']['mail']['#disabled'] = TRUE;
    $form['account']['pass']['#disabled'] = TRUE;
    $form['account']['status']['#disabled'] = TRUE;
    $form['actions']['cancel']['#access'] = FALSE;
  }
}

/**
 * Really implements hook_form_alter().
 *
 * Adds checkboxes for assignable roles to the user edit form.
 *
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function _protect_permissions_userlist_form_alter(array &$form, array &$form_state, $form_id) {
  $form['#validate'][] = '_protect_permissions_userlist_form_validate';
}

function _protect_permissions_userlist_form_validate($form, &$form_state)
{
  if (array_search($form_state['values']['operation'], array('cancel', 'block', 'unblock', 'protect_permissions_add_role', 'protect_permissions_remove_role')) !== FALSE && !empty($_POST['accounts'])) {
    $ignored = array();
    foreach ($form_state['values']['accounts'] as $uid) {
      $account = user_load($uid);
      if (count(_protect_permissions_available_roles($account->roles)) < count($account->roles) - 1) {
        $ignored[] = $account->name;
      }
    }
    if (!empty($ignored)) {
      form_set_error("options][operation", t('This operation is not allowed for the following user(s): %list.', array('%list' => implode(', ', $ignored))));
    }
  }
}

/**
 * Really implements hook_user_operations().
 *
 * Adds or removes roles to selected users.
 *
 * @return array|null
 */
function _protect_permissions_user_operations() {
  // Get roles that are available for assignment.
  $available_roles = _protect_permissions_available_roles(user_roles(TRUE));

  // Build an array of available operations.
  if (count($available_roles)) {
    $add_roles = $remove_roles = array();
    foreach ($available_roles as $key => $value) {
      $add_roles['protect_permissions_add_role-' . $key] = $value;
      $remove_roles['protect_permissions_remove_role-' . $key] = $value;
    }
    $operations = array(
      t('Add a role to the selected users') => array('label' => $add_roles),
      t('Remove a role from the selected users') => array('label' => $remove_roles),
    );
  }
  else {
    $operations = array();
  }

  // The global variable $form_values is not available anymore;
  // the $_POST values are "sanitized" below.

  // The required 'callback' key and optional 'callback arguments' key are
  // actually only needed when someone has posted. We therefore postpone
  // the attachement of these until $form_values is set.
  if (isset($_POST['operation']) && $operation = $_POST['operation']) {
    // Get operation and role id.
    $op = explode('-', $operation);
    $rid = (isset($op[1]) ? intval($op[1]) : NULL);
    $op = $op[0];

    // If not a Protect Permissions operation, there is not much to do.
    if ($op != 'protect_permissions_add_role' && $op != 'protect_permissions_remove_role' || empty($_POST['accounts'])) {
      return $operations;
    }

    // Form the name of the core callback functions for adding and
    // removing roles by choping off the 'roleassign_' part of the
    // operation string.
    $operations[$operation] = array(
      'callback'           => 'user_multiple_role_edit',
      'callback arguments' => array(substr($op, 20), $rid),
      'label'              => '(DUMMY)',
    );
  }

  return $operations;
}

/**
 * Returns the roles available to the current user.
 *
 * @param array $roles
 *   An array of all role names indexed by role IDs.
 *
 * @return array
 */
function _protect_permissions_available_roles(array $roles) {
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  $critical_perms = _protect_permissions_get_critical_permissions();
  $user_perms = array_intersect(_protect_permissions_get_user_permissions(), $critical_perms);
  $available_roles = array();
  foreach ($roles as $rid => $name) {
    $perms = user_role_permissions(array($rid => $rid));
    $perms = array_intersect(array_keys($perms[$rid]), $critical_perms);
    $locked_perms = array_diff($perms, $user_perms);
    if (empty($locked_perms)) {
      $available_roles[$rid] = $name;
    }
  }
  return $available_roles;
}

/**
 * Store and retrieve sticky roles.
 *
 * @param array|null $new_sticky_roles
 *
 * @return array
 */
function _protect_permissions_sticky_roles($new_sticky_roles = NULL) {
  static $sticky_roles = array();
  if (isset($new_sticky_roles)) {
    $sticky_roles = $new_sticky_roles;
  }
  return $sticky_roles;
}

